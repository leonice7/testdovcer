FROM nginx:alpine
ADD build/ /var/www/
ADD nginx.conf /etc/nginx/conf.d/default.conf
