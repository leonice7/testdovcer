import React from 'react';
import './App.css';
import { Route, Switch } from 'react-router';
import Home from './home';
import About from './about';
import Shop from './shop';

function App() {
  return (
    <div className="App">
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/about" exact>
            <About />
          </Route>
          <Route path="/shop" exact>
            <Shop />
          </Route>
        </Switch>
    </div>
  );
}

export default App;
