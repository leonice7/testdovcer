import React from 'react'
import { Link, Route } from 'react-router-dom';
import './header.css';

const header = () => {
    return (
        <div className="navbar">
            <div className="menu">
                <ul>
                    <li><Link style={{ textDecoration: 'none', color: 'white' }} to="/">Home</Link></li>
                    <li><Link style={{ textDecoration: 'none', color: 'white' }} to="/about">About</Link></li>
                    <li><Link style={{ textDecoration: 'none', color: 'white' }} to="/shop">Shop</Link></li>
                    <li><Link style={{ textDecoration: 'none', color: 'white' }} to="/contact">Contact</Link></li>
                </ul>
            </div>
            <div className="logo">
                Mith
            </div>
            <div className="user">
                <li><Link style={{ textDecoration: 'none', color: 'white' }} to="/login">บัญชีผู้ใช้</Link></li>
                <li><Link style={{ textDecoration: 'none', color: 'white' }} to="/support">สนับสนุน</Link></li>
                <li><Link style={{ textDecoration: 'none', color: 'white' }} to="/basket">ตระกร้าสินค้า</Link></li>
            </div>
        </div>
    )
}
export default header;
