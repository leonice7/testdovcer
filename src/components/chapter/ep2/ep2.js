import React from 'react'
import './ep2.css';

const ep2 = () => {
    return (
        <div className="mainEp2">
            <center>
                <h2> Q&A </h2>
            </center>
            <div className="item-ep2">
                <img src="/image/qa1.jpeg" className="item-ep2" />
            </div>
            <div className="item-ep2">
                <img src="/image/qa2.jpeg" className="item-ep2" />
            </div>
            <div className="item-ep2">
                <img src="/image/qa3.jpeg" className="item-ep2" />
            </div>
            <center>
            <div className="button11">
                Learn more
            </div>
            </center>
        </div>
    )
}
export default ep2;
