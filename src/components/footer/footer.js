import React from 'react'
import { Link } from 'react-router-dom';
import './footer.css';
const footer = () => {
    return (
        <div className="footer">
            <div className="left-box">
                <h3>About</h3>
            <div className="text-about">
                <p>
                e-Sports และ Fashion Life Style แทบจะเป็นของคู่กันไปแล้ว 
                ท่านสามารถหาซื้อเสื้อผ้าเครื่องแต่งกายได้ตามที่ต้องการ 
                และสามารถสนับสนุนทีมที่ท่านชอบได้ที่ร้านค้าแห่งนี้
                </p>
            </div>
            </div>
            
            <div className="right-box">
                <h3>FOOTER MENU</h3>
            <div className="menu-footer">
            <ul>
                    <li><Link style={{ textDecoration: 'none', color: 'black' }} to="/">Home</Link></li>
                    <li><Link style={{ textDecoration: 'none', color: 'black' }} to="/about">About</Link></li>
                    <li><Link style={{ textDecoration: 'none', color: 'black' }} to="/shop">Shop</Link></li>
                    <li><Link style={{ textDecoration: 'none', color: 'black' }} to="/contact">Contact</Link></li>
            </ul>
            </div>
            </div>
        </div>
    )
}
export default footer;
