import React from 'react';
import Header from './components/header/header';
import './App.css';
import Footer from './components/footer/footer';
import { Link } from 'react-router-dom';
import './css/shop.css';

function shop() {
  return (
    <div className="App">
        <Header />
          <div className="mainshop1">
            <div className="sidebar">
                <ul>
                    <p style={{ textDecoration: 'none', color: 'black', marginBottom: '15px' }}> FILTER</p>
                    <li><Link style={{ textDecoration: 'none', color: '#7B7B7B' }} to="/">Leo</Link></li>
                    <li><Link style={{ textDecoration: 'none', color: '#7B7B7B' }} to="/about">e-sports</Link></li>
                    <li><Link style={{ textDecoration: 'none', color: '#7B7B7B' }} to="/shop">t-shirt</Link></li>
                    <li><Link style={{ textDecoration: 'none', color: '#7B7B7B' }} to="/contact">case</Link></li>
                </ul>
            </div>
            <div className="content-shop">
                <div className="title-shop1">
                    <p>Leo Official</p>
                </div>
                <div className="item-shop1">
                    <img src="/image/s1.jpeg" className="item-ep1" />
                </div>
                <div className="item-shop1">
                    <img src="/image/s2.jpeg" className="item-ep1" />
                </div>
                <div className="item-shop1">
                    <img src="/image/s3.jpeg" className="item-ep1" />
                </div>
                <div className="item-shop1">
                    <img src="/image/s4.jpeg" className="item-ep1" />
                </div>
            </div>
          </div>
        <Footer />
    </div>
  );
}

export default shop;
