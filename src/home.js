import React from 'react';
import Header from './components/header/header';
import './App.css';
import Ep1 from './components/chapter/ep1/ep1';
import Ep2 from './components/chapter/ep2/ep2';
import Footer from './components/footer/footer';
import Ep3 from './components/chapter/ep3/ep3';

function home() {
  return (
    <div className="App">
        <Header />
        <img src="image/jacob.jpeg" />
        <Ep1 />
        <Ep2 />
        <Ep3 />
        <Footer />
    </div>
  );
}

export default home;
